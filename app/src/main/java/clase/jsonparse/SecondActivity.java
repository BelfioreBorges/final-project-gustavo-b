package clase.jsonparse;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView editText_title, editText_price, editText_description, editText_image;
    Button btn_back;
    Button button_update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        //get Product
        Product c = (Product)getIntent().getSerializableExtra("product");


        //FindviewByIds
        editText_title = (TextView) findViewById(R.id.title);
        editText_price = (TextView) findViewById(R.id.price);
        editText_description = (TextView) findViewById(R.id.description);
        editText_image = (TextView) findViewById(R.id.image);

        //SET TEXT
        editText_title.setText(c.title);
        editText_price.setText(c.price);
        editText_description.setText(c.description);
        editText_image.setText(c.image);

        //update

        button_update = (Button) findViewById(R.id.button_update);
        button_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        //back
        btn_back = (Button) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
